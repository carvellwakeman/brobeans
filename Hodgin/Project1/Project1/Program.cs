﻿using Project1.Objects;
using System;

namespace Project1
{
	/// <summary>
	/// NOTES:
	/// OOP / classes / accessibility modifiers / inherritence / interfaces / virtual methods / abstract methods
	/// Generics
	/// namespaces
	/// exceptions
	/// strings
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");

			#region Exceptions
			
			try
			{
				throw new Exception("Crash me!");
				//string[] arr = new string[] { };
				//var t = arr[1];
			} catch (IndexOutOfRangeException)
			{
				//_logger.Warning("You did something dumb");
				//throw;
			}
			catch (Exception)
			{

			}
			finally
			{

			}

			#endregion

			#region Environment
			//Environment.GetEnvironmentVariable("MY_VAR");
			#endregion

			#region OOP
			var sedan = new Sedan("black");

			//sedan.Drive(true);
			//sedan.Steer(true);

			//sedan.Drive(true);

			var me = new Passenger();

			sedan.SetPassengers(me, me, me);

			Console.WriteLine(sedan);

			var sedanRPM = sedan.GetRPM();
			#endregion

		}
	}
}
