﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project1.Objects
{
	public abstract class Car
	{
		protected int Wheels;

		public Car()
		{
			this.Wheels = 4;
		}

		public Car(int wheels)
		{
			this.Wheels = wheels;
		}

		public void Drive(bool forward)
		{
			// ...
		}

		public virtual void Steer(bool direction)
		{
			// ...
			// ...
		}

		public abstract void ShiftGears(short gear);
	}
}
