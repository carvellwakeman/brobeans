﻿using System;
using System.Collections.Generic;

namespace Project1.Objects
{
	public class Sedan : Car, IEngine
	{
		private string carColor;
		private Passenger[] passengers;

		public Sedan(string color) : base(4)
		{
			carColor = color;

			//Wheels = 12;
		}

		public void SetPassengers(params Passenger[] passengers)
		{
			this.passengers = passengers;
		}

		public override string ToString()
		{
			//return String.Format("I am a {0} sedan with {1} wheels", carColor, Wheels);
			//return "I am a " + carColor + " sedan with " + Wheels.ToString() + " wheels";
			return $"I am a {carColor} sedan with {Wheels} wheels";
		}

		public override void Steer(bool direction)
		{
			base.Steer(direction);
		}

		public override void ShiftGears(short gear)
		{
			//throw new NotImplementedException();
		}


		// Engine implementation
		public void SetAcceleration(double acceleration)
		{
			//throw new NotImplementedException();
		}

		public int GetRPM()
		{
			return 1;
			//throw new NotImplementedException();
		}
	}
}
