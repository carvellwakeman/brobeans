﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project1.Objects
{
	public interface IEngine
	{
		void SetAcceleration(double acceleration);
		int GetRPM();
	}
}
